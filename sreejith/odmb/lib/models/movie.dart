class MovieModel {
  var title;
  var actors;
  var runtime;
  var language;
  var plot;
  var rating;
  var votes;
  var poster;
  MovieModel(
      {this.title,
      this.actors,
      this.runtime,
      this.language,
      this.plot,
      this.rating,
      this.votes,
      this.poster});
}

class MovieNameModel {
  var name;
  MovieNameModel({this.name});
}
