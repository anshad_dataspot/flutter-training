import 'package:flutter/material.dart';
import 'package:odmb/cubic_home.dart';
import 'package:odmb/cubit/movie_cubit.dart';
import 'package:odmb/date_picker.dart';
import 'package:odmb/home_page.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(MaterialApp(
    home: DatePickerPage(),
    // BlocProvider(
    //   create: (context) => MovieCubit(CubicHome()),
    //   child: CubicHome(),
    // ),
    debugShowCheckedModeBanner: false,
  ));
}
