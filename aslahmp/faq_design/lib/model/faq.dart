class Faq {
  Faq({
    required this.answer,
    required this.question,
    this.isExpanded = false,
  });

  String answer;
  String question;
  bool isExpanded;
  void toggleExpanded() {
    isExpanded = !isExpanded;
  }
}
