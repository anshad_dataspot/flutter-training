import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);



  @override
  Widget build(BuildContext context) {
    return MediaQuery(
      data: const MediaQueryData(),
      child: MaterialApp(
           home: DefaultTabController(
            length: 2,
            child: Scaffold(
              appBar: AppBar(
                backgroundColor: Colors.white,
                bottom: TabBar(
                  tabs: <Widget>[
                    Tab(
                      child: Text ('Active Order',
                      style: TextStyle(
                        color: Colors.black,
                      ),
                      )
                    ),
                    Tab(
                        child: Text ('Upcoming Order',
                          style: TextStyle(
                            color: Colors.black,
                          ),
                        ),
                    ),
                  ],
                ),
              ),
              body: Column(
                children: <Widget>[
                  Container(
                    child: Column(
                      children: <Widget>[

                        Row(
                          children: <Widget>[
                            Text(' Order #749',
                            style: TextStyle(
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold,
                            ),),
                            SizedBox(
                              height: 0.0,
                              width: 240,
                            ),
                            Icon(Icons.keyboard_arrow_down),
                          ],

                        ),

                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: <Widget>[
                            Text(' Qty',
                            style: TextStyle(
                              color: Colors.black38,
                            ),
                            ),
                            SizedBox(
                              width: 15.0,
                            ),
                            Text('50L'),
                          ],
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Row(
                          children: <Widget>[
                            Text(' ETD',
                            style: TextStyle(
                              color: Colors.black38,
                            ),
                            ),
                            SizedBox(
                              width: 15.0,
                            ),
                            Text('Today 11:00 PM'),
                          ],
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Row(
                          children: <Widget>[
                            Text(' Order by',
                              style: TextStyle(
                                color: Colors.black38,
                              ),
                            ),
                            SizedBox(
                              width: 15.0,
                            ),
                            Text('Accounts Manager'),
                          ],
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Row(
                          children: <Widget>[
                            Text(' Delivery Point',
                            style: TextStyle(
                              color: Colors.black38,
                            ),),
                            SizedBox(
                              width: 11.0,
                            ),
                            RaisedButton(
                              onPressed:() {},
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0),
                              ),
                              color: Colors.black12,
                              child: Row(
                                children: <Widget>[
                                 Icon(Icons.inbox),
                                  SizedBox(
                                    width: 10.0,
                                  ),
                                  Text("BHL Industry"),
                                ],
                              ),
                            ),
                          ],
                        ),

                      ],
                    ),
                  margin: EdgeInsets.all(10.0),
                  decoration: BoxDecoration(
                    color: Colors.black12,
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  height: 150,
                  width: 360,
                ),


              Container(
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text(' Order #749',
                          style: TextStyle(
                            fontSize: 15.0,
                            fontWeight: FontWeight.bold,
                          ),),
                        SizedBox(
                          height: 0.0,
                          width: 240,
                        ),
                        Icon(Icons.keyboard_arrow_down),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        Text(' Qty',
                          style: TextStyle(
                            color: Colors.black38,
                          ),
                        ),
                        SizedBox(
                          width: 15.0,
                        ),
                        Text('50L'),
                      ],
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                    Row(
                      children: <Widget>[
                        Text(' ETD',
                          style: TextStyle(
                            color: Colors.black38,
                          ),
                        ),
                        SizedBox(
                          width: 15.0,
                        ),
                        Text('Today 11:00 PM'),
                      ],
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                    Row(
                      children: <Widget>[
                        Text(' Delivery Point',
                          style: TextStyle(
                            color: Colors.black38,
                          ),),
                        SizedBox(
                          width: 11.0,
                        ),
                        RaisedButton(
                          onPressed:() {},
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                          color: Colors.black12,
                          child: Row(
                            children: <Widget>[
                              Icon(Icons.inbox),
                              SizedBox(
                                width: 10.0,
                              ),
                              Text("BHL Industry"),

                            ],

                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                margin: EdgeInsets.all(10.0),
                decoration: BoxDecoration(
                  color: Colors.black12,
                  borderRadius: BorderRadius.circular(10.0),
                ),
                height: 130,
                width: 360,
              ),



                  Container(
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Text(' Order #749',
                              style: TextStyle(
                                fontSize: 15.0,
                                fontWeight: FontWeight.bold,
                              ),),
                            SizedBox(
                              height: 0.0,
                              width: 240,
                            ),
                            Icon(Icons.keyboard_arrow_down),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: <Widget>[
                            Text(' Qty',
                              style: TextStyle(
                                color: Colors.black38,
                              ),
                            ),
                            SizedBox(
                              width: 15.0,
                            ),
                            Text('50L'),
                          ],
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Row(
                          children: <Widget>[
                            Text(' ETD',
                              style: TextStyle(
                                color: Colors.black38,
                              ),
                            ),
                            SizedBox(
                              width: 15.0,
                            ),
                            Text('Today 11:00 PM'),
                          ],
                        ),
                        SizedBox(
                          height: 10.0,
                        ),

                        Row(
                          children: <Widget>[
                            Text(' Delivery Point',
                              style: TextStyle(
                                color: Colors.black38,
                              ),),
                            SizedBox(
                              width: 11.0,
                            ),
                            RaisedButton(
                              onPressed:() {},
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0),
                              ),
                              color: Colors.black12,
                              child: Row(
                                children: <Widget>[
                                  Icon(Icons.inbox),
                                  SizedBox(
                                    width: 10.0,
                                  ),
                                  Text("BHL Industry"),

                                ],

                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    margin: EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                      color: Colors.black12,
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    height: 130,
                    width: 360,
                  ),



                  Container(
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Text(' Order #749',
                              style: TextStyle(
                                fontSize: 15.0,
                                fontWeight: FontWeight.bold,
                              ),),
                            SizedBox(
                              height: 0.0,
                              width: 240,
                            ),
                            Icon(Icons.keyboard_arrow_down),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: <Widget>[
                            Text(' Qty',
                              style: TextStyle(
                                color: Colors.black38,
                              ),
                            ),
                            SizedBox(
                              width: 15.0,
                            ),
                            Text('50L'),
                          ],
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Row(
                          children: <Widget>[
                            Text(' ETD',
                              style: TextStyle(
                                color: Colors.black38,
                              ),
                            ),
                            SizedBox(
                              width: 15.0,
                            ),
                            Text('Today 11:00 PM'),
                          ],
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Row(
                          children: <Widget>[
                            Text(' Delivery Point',
                              style: TextStyle(
                                color: Colors.black38,
                              ),),
                            SizedBox(
                              width: 11.0,
                            ),
                            RaisedButton(
                              onPressed:() {},
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0),
                              ),
                              color: Colors.black12,
                              child: Row(
                                children: <Widget>[
                                  Icon(Icons.inbox),
                                  SizedBox(
                                    width: 10.0,
                                  ),
                                  Text("BHL Industry"),
                                ],

                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    margin: EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                      color: Colors.black12,
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    height: 130,
                    width: 360,
                  ),


                ],
              ),
              ),
            ),
          ),
        );

  }
}

