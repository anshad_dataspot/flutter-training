import 'package:flutter/material.dart';
import 'package:movie_api/key_work.dart';
import 'model/movie.dart';


class SingleMovie extends StatelessWidget {
  Movie movie;
  SingleMovie(this.movie);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          movie.title,
          style: const TextStyle(
            fontSize: 25,
          ),
        ),
      ),
      body:
          ListView(
        children: [
          Container(
            padding: const EdgeInsets.all(20),
            child: Image.network(
              movie.poster,
              height: 400,
              fit: BoxFit.fitHeight,
            ),
          ),

          SizedBox(
            height: 40,
          ),

          details('Directed by', movie.director),
          details('Produced by', movie.production),
          details('Release date', movie.year),
          details('Rating', movie.rating),
          details('Actors', movie.actors),
          details('Screenplay', movie.writer),
          details('Language', movie.language),
        ],
      ),
    );
  }
}


Widget details(String tittle, String details) {
  return Container(
    margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          tittle,
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.w900),
        ),
        SizedBox(
          height: 10,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 0),
          child: Text(
            details,
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
          ),
        ),
      ],
    ),
  );
}