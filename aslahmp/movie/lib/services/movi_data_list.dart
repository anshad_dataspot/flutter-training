import 'dart:collection';

class MoviNameList {
  final List<String> _moveName = [
    'abcd',
    'Cruella',
    'A Quiet Place Part II',
    'Wrath of Man',
    'Spiral: From the Book of Saw',
    'The Woman in the Window',
    ' Vertigo',
    'Notorious',
    'The Godfather',
  ];

  UnmodifiableListView<String> get movies {
    return UnmodifiableListView(_moveName);
  }
}
