import 'package:http/http.dart' as http;
import 'dart:convert';

const String key = 'dd11a59e';
const String url = 'https://www.omdbapi.com/?apikey=';

class NetworkHelper {
  String key = 'dd11a59e';
  String url = 'https://www.omdbapi.com/?apikey=';
  Future getMovie(String name) async {
    try {
      http.Response response = await http.get(
        Uri.parse('$url$key&t=$name'),
      );
      if (response.statusCode == 200) {
        String data = response.body;

        return jsonDecode(data);
      } else {
        // ignore: avoid_print
        print(response.statusCode);
        return Error;
      }
    } catch (e) {
      return Error;
    }
  }
}
