import 'package:faq_design/model/faq_data.dart';
import 'package:faq_design/screen/faq_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static const String _title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (BuildContext context) => FaqData(),
      child: MaterialApp(
        home: FaqScreen(),
      ),
    );
  }
}

// stores ExpansionPanel state information


