import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(

        backgroundColor: Colors.teal,
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircleAvatar(
                radius: 40.0,
                backgroundImage: AssetImage('images/nafil.jpg'),

              ),
              Text(
                'NAFIL',
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,

                    ),

              ),
              Text(
                'Flutter Developer',
                 style: TextStyle(
                   fontSize: 19.0,
                   fontStyle: FontStyle.italic,
                   color: Colors.black54,
                 ),
              ),
              SizedBox(
                height: 20.0,
                width: 120.0,
                child: Divider(
                  color: Colors.black45,
                ),

              ),
              Card(
                color: Colors.white,
                margin: EdgeInsets.symmetric(vertical: 10.0,horizontal: 25.0),
                child:ListTile(
                  leading:Icon(Icons.call,
                    color: Colors.black45,
                  ),
                  title: Text('+91 7902217526',
                    style: TextStyle(
                      color: Colors.black,
                    ),
                  ),
                )
              ),
              Card(
                color: Colors.white,
                margin: EdgeInsets.symmetric(vertical: 10.0,horizontal: 25.0),
                child: ListTile(
                  leading: Icon(Icons.email,
                    color: Colors.black45,
                  ),
                  title: Text('nafilm64@gmail.com',
                    style: TextStyle(
                      color: Colors.black,
                    ),
                  ),
                )
              )
            ],
          ),

        )
      ),
    );
  }
}
