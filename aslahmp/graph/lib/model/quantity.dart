class Quantity {
  final String month;
  final int id;
  final double value;
  Quantity({required this.id, required this.month, required this.value});
}
