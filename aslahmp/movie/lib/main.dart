import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie/cubit/movie_cubit.dart';
import 'package:movie/screens/home_screen.dart';
import 'package:movie/services/networking.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: BlocProvider(
      create: (context) => MovieCubit(NetworkHelper()),
      child: HomePage(),
    ));
  }
}
