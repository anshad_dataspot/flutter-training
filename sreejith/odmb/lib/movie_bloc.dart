import 'dart:async';
import 'package:odmb/models/movie.dart';
import 'package:odmb/movie_event.dart';
import 'package:omdb_dart/omdb_dart.dart';

import 'home_page.dart';

class MovieBloc {
  final _movieStateController = StreamController<List<MovieModel>>();
  StreamSink<List<MovieModel>> get _inMovies => _movieStateController.sink;
  Stream<List<MovieModel>> get movires => _movieStateController.stream;

  getMovie() async {
    List<MovieModel> movies = [];
    for (int i = 0; i < movieNames.length; i++) {
      Omdb client = Omdb("be4ab7ee", ((movieNames[i] as MovieNameModel).name));
      await client.getMovie();
      movies.add(MovieModel(
          title: client.movie.title,
          actors: client.movie.actors,
          runtime: client.movie.runtime,
          language: client.movie.language,
          plot: client.movie.plot,
          rating: client.movie.imdbRating,
          votes: client.movie.imdbVotes,
          poster: client.movie.poster));
    }
    _inMovies.add(movies);
  }
}
