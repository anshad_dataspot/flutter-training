import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_api/screenn.dart';

import 'cubit/movie_cubit.dart';
import 'model/movie.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);



  @override
  _HomePageState createState() => _HomePageState();
}


class _HomePageState extends State<HomePage> {
  void onRefresh() async {
    BlocProvider.of<MovieCubit>(context).getData();
  }

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<MovieCubit>(context).getData();
    return  Scaffold(
      appBar: AppBar(
        title: Text('Movie List'),
        actions: [
          IconButton(onPressed: onRefresh, icon: const Icon(Icons.refresh))
        ],
      ),

      body: BlocBuilder<MovieCubit, MovieState>(
        builder: (context, state) {
          if (state is MovieLoaded) {
            List<Movie> _movie = state.movies;
            return ListView.builder(
                itemCount: _movie.length,
                itemBuilder: (context, index) {
                  return MovieTile(
                    _movie[index],
                  );
                });
          } else if (state is MovieError) {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.error,
                    size: 100,
                    color: Colors.red.shade300,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20, vertical: 10),
                    child: Text(
                      'Something went wrong please try again',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 25,
                        color: Colors.red.shade300,
                      ),
                    ),
                  ),
                ],
              ),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}
