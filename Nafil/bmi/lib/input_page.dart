import 'package:bmi/results_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'reusable_card.dart';
import 'constants.dart';
import 'results_page.dart';
import 'bottom_button.dart';
import 'round_icon_button.dart';
import 'calculator_brain.dart';

enum Gender{
  male,
  female,
}
class InputPage extends StatefulWidget {
  @override
  _InputPageState createState() => _InputPageState();
}
class _InputPageState extends State<InputPage> {
  Gender selectedGender= Gender.male;
  int height = 180;
  int weight = 60;
  int age = 20;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text('BMI CALCULATOR',)),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch ,
       children: <Widget>[
         Expanded(child: Row(
           children: <Widget>[
             Expanded(child: ReusableCard(
               onPress: () {
                 setState(() {
                   selectedGender = Gender.male;
                 });
               },
               colour: selectedGender == Gender.male ? kActivecardcolour : kInactivecardcolour,
               cardChild: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                 Icon(
                   FontAwesomeIcons.mars,
                   size: 80.0,
                 ),
                 SizedBox(
                   height: 15.0,
                 ),
                 Text('MALE',style: kTextstylee,),
               ],
             ),
             ),
             ),
              Expanded(
                child:ReusableCard(
                  onPress: () {
                    setState(() {
                     selectedGender = Gender.female;
                   });

                  },
                colour: selectedGender == Gender.female ? kActivecardcolour : kInactivecardcolour,
                cardChild: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      FontAwesomeIcons.venus,
                      size: 80.0,
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    Text('FEMALE',style: kTextstylee),
                  ],
                ),
              ),
             ),
           ],
         ),
         ),
              Expanded(child: ReusableCard
                (colour: kActivecardcolour,
                 cardChild: Column(
                   mainAxisAlignment: MainAxisAlignment.center,
                   children: <Widget>[
                     Text(
                       'HEIGHT',style: kTextstylee,
                     ),
                     Row(
                       mainAxisAlignment: MainAxisAlignment.center,
                       crossAxisAlignment: CrossAxisAlignment.baseline,
                       textBaseline: TextBaseline.alphabetic,
                       children: <Widget>[
                         Text(height.toString(), style: kNumberTextstylee),
                         Text('cm', style: kTextstylee,)
                       ],
                     ),
                     SliderTheme(
                       data: SliderTheme.of(context).copyWith(
                         inactiveTrackColor: Color(0xFF8D8E98),
                         activeTrackColor: Colors.white,
                         thumbColor: Color(0xFFEB1555),
                         overlayColor: Color(0x29EB1555),
                         thumbShape: RoundSliderThumbShape(enabledThumbRadius: 15.0),
                         overlayShape: RoundSliderOverlayShape(overlayRadius: 30.0),
                       ),
                       child: Slider(value: height.toDouble(),
                           min: 120.0,
                           max: 220.0,
                           activeColor: Colors.white,
                           inactiveColor: Color(0xFF8D8E98),
                           onChanged: (double value) {
                           setState(() {
                             height = value.round();
                           });

                           },
                           ),
                     )
                   ],
                 ), onPress: () {},
              ),
         ),
              Expanded(child: Row(
                children: <Widget>[
                    Expanded(
                      child: ReusableCard(
                          colour:kActivecardcolour,
                          cardChild: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('WEIGHT', style: kTextstylee),
                                Text(
                                    weight.toString(),
                                    style: kNumberTextstylee,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    RoundIconButton(
                                        icon: FontAwesomeIcons.minus,
                                      onPressed: (){
                                          setState(() {
                                            weight--;
                                          });
                                      },
                                    ),
                                    SizedBox(
                                      width: 10.0,
                                    ),
                                    RoundIconButton(
                                      icon: FontAwesomeIcons.plus,
                                      onPressed: (){
                                        setState(() {
                                          weight++;
                                        });
                                      },
                                    ),
                                  ],
                               ),
                          ],
                      ), onPress: (){},
                      ),
                    ),
              Expanded(
                child: ReusableCard(
                    colour: kActivecardcolour,
                         cardChild: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                           children: <Widget>[
                              Text('AGE', style: kTextstylee),
                              Text(
                               age.toString(),
                               style: kNumberTextstylee,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  RoundIconButton(
                                     icon: FontAwesomeIcons.minus,
                                       onPressed: (){
                                        setState(() {
                                           age--;
                                        });
                                       },
                                     ),
                               SizedBox(
                                 width: 10.0,
                               ),
                              RoundIconButton(
                                icon: FontAwesomeIcons.plus,
                                 onPressed: (){
                                  setState(() {
                                      age++;
                                 });
                               },
                              ),
                         ],
                       ),],
                      ), onPress: (){},
                ),
              ),
           ],
         ),
       ),
       BottomButton(buttontitle: 'CALCULATE', onTap: (){
         CalculatorBrain calc = CalculatorBrain(height: height,weight: weight);

         Navigator.push(context, MaterialPageRoute(builder: (context) => ResultsPage(
           bmiResult: calc.calculateBMI(),
           resultText: calc.getResult(),
           interpretations: calc.getInterpretation(),
         )));
       },
       ),
    ],
     ),
    );
  }
}



