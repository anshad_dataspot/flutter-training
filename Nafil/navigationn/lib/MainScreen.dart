import 'package:flutter/material.dart';
import 'package:navigationn/SecondScreen.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(icon: Icon(Icons.menu), onPressed: (){
          //
        }),
        title: Text('Main Screen'),
        actions: <Widget>[
      IconButton(icon: Icon(Icons.search), onPressed: (){
        //
    }),
        ],
      ),
    body: Center(
      child: RaisedButton(
        child: Text('GO',
        style: TextStyle(
            color: Colors.blue,
        ),),

        onPressed: () {
    Navigator.push(context,
    MaterialPageRoute(builder: (context)=>SecondScreen()),
    );
    },

      ),

    ),
    );
  }
}
