import 'package:flutter/material.dart';

void main(){
  runApp(
    MaterialApp(
      title: 'Favorite colour',
      home: FavoriteColor(),
    )
  );
}
class FavoriteColor extends StatefulWidget {
  const FavoriteColor({Key? key}) : super(key: key);

  @override
  _FavoriteColorState createState() => _FavoriteColorState();
}

class _FavoriteColorState extends State<FavoriteColor> {
  
  String FavoriteColorr = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('What is your favorite colour??'),
        
      ),
      body: Container(
        margin: EdgeInsets.all(20.0),
        child: Column(
          children: <Widget>[
            TextField(
              onSubmitted: (String userInput) {

                setState(() {
                  FavoriteColorr = userInput;
                });
              },
            ),
            Padding(
                padding: EdgeInsets.all(30.0),
                child: Text(
              'Your favourite colour is $FavoriteColorr',
              style: TextStyle(fontSize: 20.0),
            ))
            
          ],
        ),
      ),
    );
  }
}

