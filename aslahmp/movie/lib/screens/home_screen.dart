import 'package:flutter/material.dart';

import 'package:movie/cubit/movie_cubit.dart';
import 'package:movie/model/movie.dart';
import 'package:movie/widget/movie_tile.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

// ignore: use_key_in_widget_constructors
class HomePage extends StatefulWidget {
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  void onRefresh() async {
    BlocProvider.of<MovieCubit>(context).getData();
  }

  // final _bloc = MovieBloc();
  @override
  void initState() {
    super.initState();
    // _bloc.getData();
  }

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<MovieCubit>(context).getData();
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Movies',
          style: TextStyle(
            fontSize: 25,
          ),
        ),
        actions: [
          IconButton(onPressed: onRefresh, icon: const Icon(Icons.refresh))
        ],
      ),
      body: BlocBuilder<MovieCubit, MovieState>(
        builder: (context, state) {
          if (state is MovieLoaded) {
            List<Movie> _movie = state.movies;
            return ListView.builder(
                itemCount: _movie.length,
                itemBuilder: (context, index) {
                  return MovieTile(
                    movie: _movie[index],
                  );
                });
          } else if (state is MovieError) {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.error,
                    size: 100,
                    color: Colors.red.shade300,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20, vertical: 10),
                    child: Text(
                      'Something went wrong please try again',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 25,
                        color: Colors.red.shade300,
                      ),
                    ),
                  ),
                ],
              ),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }

  @override
  void dispose() {
    // _bloc.close();
    super.dispose();
  }
}
