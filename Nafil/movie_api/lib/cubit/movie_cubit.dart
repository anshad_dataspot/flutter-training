import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:movie_api/key_work.dart';
import '../model/movie.dart';
import 'package:movie_api/movie_list.dart';
import '../movie_list.dart';
import '../movie_list.dart';
import 'package:meta/meta.dart';

import '../movie_list.dart';
import '../movie_list.dart';

part 'movie_state.dart';

class MovieCubit extends Cubit<MovieState> {
  NetworkHelper networkHelper;

  // ignore: prefer_final_fields
  List<Movie> _movies = [];

  MovieCubit(this.networkHelper) : super(MovieLoading());
  void getData() async {
    emit(MovieLoading());

    var movieList = MovieNameList().movies;
    for (int i = 0; i < movieList.length; i++) {
      var movieData = await networkHelper.getMovie(movieList[i]);
      if (movieData == Error) {
        emit(MovieError());

        return;
      } else {
        Movie movie = Movie.fromJson(movieData);
        _movies.add(movie);
      }
    }
    // _inMovies.add(_movies);
    emit(MovieLoaded(_movies));

  }
}
