import 'package:flutter/material.dart';

import 'package:odmb/models/movie.dart';
import 'package:odmb/movie_event.dart';
import 'package:odmb/movie_list.dart';
import 'package:omdb_dart/omdb_dart.dart';
import 'package:odmb/movie_bloc.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

//List<MovieModel> movies = [];

var i;
List movieNames = [];
int? a;
int? b;

class _HomePageState extends State<HomePage> {
  final _bloc = MovieBloc();
  @override
  void initState() {
    movieNameAdd();
    _bloc.getMovie();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[900],
      appBar: AppBar(
        title: Text('Movie List'),
        backgroundColor: Colors.grey[850],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          returnListview(),
        ],
      ),
    );
  }

  movieNameAdd() {
    movieNames.add(MovieNameModel(name: "Batman"));

    movieNames.add(MovieNameModel(name: "Parasite"));
    movieNames.add(MovieNameModel(name: "Loki"));
    movieNames.add(MovieNameModel(name: "Avengers"));
    movieNames.add(MovieNameModel(name: "Death Race"));
    movieNames.add(MovieNameModel(name: "Don't Breathe"));
    movieNames.add(MovieNameModel(name: "Army Of The Dead"));
    movieNames.add(MovieNameModel(name: "The Mechanic"));
  }

  returnListview() {
    return Container(
        child: StreamBuilder<Object>(
            stream: _bloc.movires,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                List<MovieModel> movies = snapshot.data as List<MovieModel>;
                return Expanded(
                    child: ListView.separated(
                        separatorBuilder: (context, i) => Divider(
                              height: 30,
                              color: Colors.grey[300],
                            ),
                        itemCount: movies.length,
                        itemBuilder: (context, i) {
                          return ListTile(
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => MovieList(
                                        movie: movies[i],
                                      )));
                            },
                            leading: Container(
                              height: 100,
                              width: 50,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: NetworkImage(
                                          (movies[i] as MovieModel).poster))),
                            ),
                            title: Text(
                              (movies[i] as MovieModel).title,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 17,
                                  fontWeight: FontWeight.bold),
                            ),
                            contentPadding: EdgeInsets.only(top: 5),
                            subtitle: Text(
                                'Actors : ${(movies[i] as MovieModel).actors}',
                                style: TextStyle(
                                    color: Colors.grey[350],
                                    fontStyle: FontStyle.italic)),
                          );
                        }));
              } else {
                return Center(
                  child: Padding(
                    padding: const EdgeInsets.all(100),
                    child: CircularProgressIndicator(),
                  ),
                );
              }
            }));
  }
}
