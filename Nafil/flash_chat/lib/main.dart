import 'package:flash_chat/screens/chat_screen.dart';
import 'package:flash_chat/screens/login_screen.dart';
import 'package:flash_chat/screens/registration_screen.dart';
import 'package:flash_chat/screens/welcome_screen.dart';
import 'package:flutter/material.dart';

void main() => runApp(FlashChat());

class FlashChat extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(

      initialRoute: WelcomeScreen.id1,
      routes: {
        WelcomeScreen.id1: (context) => WelcomeScreen(),
        LoginScreen.id2 : (context)  => LoginScreen(),
        RegistrationScreen.id3 : (context) => RegistrationScreen(),
        ChatScreen.id4 : (context) => ChatScreen(),
      },
    );
  }
}
