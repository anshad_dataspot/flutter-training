import 'package:flutter/material.dart';
import 'package:graph/model/quantity.dart';

class BarData {
  static int interval = 5;

  static List<Quantity> barData = [
    Quantity(id: 1, month: 'Jan', value: 70),
    Quantity(id: 2, month: 'Feb', value: 110),
    Quantity(id: 3, month: 'Mar', value: 190),
    Quantity(id: 4, month: 'Apr', value: 205),
    Quantity(id: 5, month: 'May', value: 140),
    Quantity(id: 6, month: 'Jun', value: 205),
    Quantity(id: 7, month: 'Jul', value: 80),
    Quantity(id: 8, month: 'Aug', value: 220),
    Quantity(id: 9, month: 'Sep', value: 170),
    Quantity(id: 10, month: 'Oct', value: 130),
    Quantity(id: 11, month: 'Nov', value: 50),
    Quantity(id: 12, month: 'Dec', value: 60),
  ];
}
