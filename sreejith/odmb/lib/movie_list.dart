import 'package:flutter/material.dart';
import 'package:omdb_dart/model/movie.dart';

import 'models/movie.dart';

class MovieList extends StatelessWidget {
  var movie;
  MovieList({this.movie});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[900],
        body: SingleChildScrollView(
          child: Container(
              // margin: EdgeInsets.all(30),
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(padding: EdgeInsets.all(40)),
              sectionOne(),
              Padding(padding: EdgeInsets.only(top: 30)),
              fields(descripton: movie.plot, heading: 'INFO'),
              fields(descripton: movie.actors, heading: 'ACTORS'),
              fields(descripton: movie.language, heading: 'LANGUAGE'),
              fields(descripton: movie.runtime, heading: 'DURATION')
            ],
          )),
        ));
  }

  sectionOne() {
    return Container(
      //color: Colors.red,
      height: 250,
      width: double.infinity,
      child: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(left: 80),
            alignment: Alignment.center,
            height: 200,
            width: 200,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                image: DecorationImage(
                    image: NetworkImage(movie.poster), fit: BoxFit.fill)),
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: Padding(
              padding: EdgeInsets.only(right: 85, bottom: 20),
              child: Text(
                'Votes  : ' + movie.votes,
                style:
                    TextStyle(color: Colors.white, fontStyle: FontStyle.italic),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomLeft,
            child: Padding(
                padding: EdgeInsets.only(left: 80, bottom: 20),
                child: Row(
                  children: [
                    Icon(
                      Icons.star,
                      color: Colors.yellow,
                    ),
                    Padding(padding: EdgeInsets.only(left: 5)),
                    Text(
                      movie.rating,
                      style: TextStyle(
                          color: Colors.white, fontStyle: FontStyle.italic),
                    ),
                  ],
                )),
          )
        ],
      ),
    );
  }

  fields({var heading, var descripton}) {
    return Container(
      margin: EdgeInsets.all(30),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            heading,
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
          Padding(padding: EdgeInsets.only(top: 15)),
          Text(descripton,
              style: TextStyle(
                  color: Colors.grey[350], fontStyle: FontStyle.italic)),
          Padding(padding: EdgeInsets.only(top: 10)),
        ],
      ),
    );
  }
}
