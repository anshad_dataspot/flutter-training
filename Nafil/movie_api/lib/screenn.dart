import 'package:flutter/material.dart';
import 'package:movie_api/model/movie.dart';
import 'Desc_screen.dart';

class MovieTile extends StatelessWidget {

  Movie movie;
  MovieTile(this.movie);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        child:
        Container(
        margin: EdgeInsets.only(left: 10, right: 10, bottom: 5, top: 5),
    child: Row(
    crossAxisAlignment: CrossAxisAlignment.start,
    mainAxisAlignment: MainAxisAlignment.start,
    children: [
    Image.network(
    movie.poster,
    height: 100,
    ),
    Expanded(
    child: Container(
    margin: EdgeInsets.only(left: 10, top: 20),
    child: Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [

    Text(
    movie.title,
    style: TextStyle(
    fontSize: 20, fontWeight: FontWeight.w300),
    ),
    Text(
    'Release: ${movie.year}',
    style: TextStyle(fontSize: 16),
    ),
    Text(
    'Director: ${movie.director}',
    style: TextStyle(fontSize: 16),
    ),
    Text(
    '⭐${movie.rating}',
    style: TextStyle(
    fontSize: 16, fontWeight: FontWeight.w400),
    ),
    ],
    ),
    )
    )
    ],
    ),
    ),
      onTap:() {
        Navigator.push(context, MaterialPageRoute(builder: (context) {
          return SingleMovie(movie);
        }),
        );
      }
  );
  }
}


