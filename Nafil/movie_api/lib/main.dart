import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_api/screenn.dart';
import 'homepage.dart';
import 'key_work.dart';
import 'model/movie.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_api/cubit/movie_cubit.dart';


void main() {
  runApp(MyApp());

}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  //final _bloc = CounterBloc();

  @override
  void initState() {
    //_bloc.getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    //BlocProvider.of<MovieCubit>(context).getData();
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData.dark(),
      title: 'Movies App',
      home: BlocProvider(
        create: (context) => MovieCubit(NetworkHelper()),
        child:HomePage(),
      ),
    );
  }
}


