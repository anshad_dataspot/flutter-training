import 'package:flutter/foundation.dart';
import 'dart:collection';
import './faq.dart';

class FaqData extends ChangeNotifier {
  final List<Faq> _faqs = [
    Faq(
      answer:
          'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type',
      question:
          'Lorem Ipsum is simply dummy text of the printing and typesettin',
    ),
    Faq(
      answer: 'There are many variations of passages of Lorem',
      question:
          'Lorem Ipsum is simply dummy text of the printing and typesettin',
    ),
    Faq(
      answer: 'There are many variations of passages of Lorem',
      question:
          'Lorem Ipsum is simply dummy text of the printing and typesettin',
    ),
    Faq(
      answer: 'There are many variations of passages of Lorem',
      question:
          'Lorem Ipsum is simply dummy text of the printing and typesettin',
    ),
    Faq(
      answer: 'There are many variations of passages of Lorem',
      question:
          'Lorem Ipsum is simply dummy text of the printing and typesettin',
    ),
  ];

  UnmodifiableListView<Faq> get faqs {
    return UnmodifiableListView(_faqs);
  }

  int faqCount() {
    return _faqs.length;
  }

  void updateExpanded(Faq faq) {
    faq.toggleExpanded();
    notifyListeners();
  }
}
