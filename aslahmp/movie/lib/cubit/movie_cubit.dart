import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import '../services/movi_data_list.dart';
import '../services/networking.dart';
import '../model/movie.dart';
// import 'package:meta/meta.dart';

part 'movie_state.dart';

class MovieCubit extends Cubit<MovieState> {
  NetworkHelper networkHelper;

  // ignore: prefer_final_fields
  List<Movie> _movies = [];

  MovieCubit(this.networkHelper) : super(MovieLoading());
  void getData() async {
    emit(MovieLoading());
   
    var movieList = MoviNameList().movies;
    for (int i = 0; i < movieList.length; i++) {
      var moviData = await networkHelper.getMovie(movieList[i]);
      if (moviData == Error) {
        emit(MovieError());
        
        return;
      } else {
        Movie movie = Movie.fromJson(moviData);
        _movies.add(movie);
      }
    }
    // _inMovies.add(_movies);
    emit(MovieLoaded(_movies));
    
  }
}
