import 'package:flutter/material.dart';
import 'package:odmb/cubit/movie_cubit.dart';
import 'package:odmb/models/movie.dart';
import 'package:odmb/movie_list.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CubicHome extends StatefulWidget {
  @override
  _CubicHomeState createState() => _CubicHomeState();
}

class _CubicHomeState extends State<CubicHome> {
  @override
  void initState() {
    BlocProvider.of<MovieCubit>(context).getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[900],
      appBar: AppBar(
        title: Text('Movie List'),
        backgroundColor: Colors.grey[850],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          returnListview(),
        ],
      ),
    );
  }

  returnListview() {
    return Container(
      child: BlocBuilder<MovieCubit, MovieState>(
        builder: (context, state) {
          if (state is MovieLoaded) {
            List<MovieModel> movies = state.movie;
            return Expanded(
                child: ListView.separated(
                    separatorBuilder: (context, i) => Divider(
                          height: 30,
                          color: Colors.grey[300],
                        ),
                    itemCount: movies.length,
                    itemBuilder: (context, i) {
                      return ListTile(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => MovieList(
                                    movie: movies[i],
                                  )));
                        },
                        leading: Container(
                          height: 100,
                          width: 50,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: NetworkImage(
                                      (movies[i] as MovieModel).poster))),
                        ),
                        title: Text(
                          (movies[i] as MovieModel).title,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontWeight: FontWeight.bold),
                        ),
                        contentPadding: EdgeInsets.only(top: 5),
                        subtitle: Text(
                            'Actors : ${(movies[i] as MovieModel).actors}',
                            style: TextStyle(
                                color: Colors.grey[350],
                                fontStyle: FontStyle.italic)),
                      );
                    }));
          } else {
            return Center(
              child: Padding(
                padding: const EdgeInsets.all(100),
                child: CircularProgressIndicator(),
              ),
            );
          }
        },
      ),
    );
  }
}
