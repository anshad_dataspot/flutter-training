

import 'dart:collection';

class MovieNameList {

  List<String> _movieName = [
    'THE SHAWSHANK REDEMPTION',
    'THE GODFATHER',
    'THE DARK KNIGHT',
    '12 ANGRY MEN',
    'SCHINDLER\'S LIST',
    'THE LORD OF THE RINGS: THE RETURN OF THE KING',
    'PULP FICTION',
  ];

  UnmodifiableListView<String> get movies {
    return UnmodifiableListView(_movieName);
  }
}

