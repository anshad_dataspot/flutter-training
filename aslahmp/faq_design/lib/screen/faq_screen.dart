import 'package:faq_design/widgets/faq_list.dart';
import 'package:flutter/material.dart';
import '../constance.dart';

// ignore: use_key_in_widget_constructors
class FaqScreen extends StatefulWidget {
  @override
  _FaqScreenState createState() => _FaqScreenState();
}

class _FaqScreenState extends State<FaqScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: kItemColor1,
          ),
          onPressed: () {},
        ),
        title: Text(
          'FAQ',
          style: TextStyle(fontSize: 16, color: kItemColor1),
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Text(
              'How can we help you?',
              style: TextStyle(fontSize: 24, height: 1.7),
            ),
            margin: kBasicMargin.copyWith(left: 0, top: 25, bottom: 30),
            padding: kBasicMargin.copyWith(right: 158),
          ),
          Expanded(
            child: Container(
              child: buildPanel(),
            ),
          ),
        ],
      ),
    );
  }
}
