import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:todo/screen/tasks_screen.dart';

import 'model/task_data.dart';

void main() => runApp(MyApp());

// ignore: use_key_in_widget_constructors
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (BuildContext context) => TaskData(),
      child: MaterialApp(
        home: TasksScreen(),
      ),
    );
  }
}
