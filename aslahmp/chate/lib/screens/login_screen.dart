import '../components/rounded_button.dart';
import 'package:flutter/material.dart';
import '../constants.dart';
import './chat_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';

// ignore: use_key_in_widget_constructors
class LoginScreen extends StatefulWidget {
  static String id = 'login_screen';
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool isLoading = false;
  final _auth = FirebaseAuth.instance;
  String email = '';
  String password = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24.0),
        child: isLoading
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Flexible(
                    child: Hero(
                      tag: 'logo',
                      // ignore: sized_box_for_whitespace
                      child: Container(
                        height: 200.0,
                        child: Image.asset('images/logo.png'),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 48.0,
                  ),
                  TextField(
                      onChanged: (value) {
                        email = value;
                      },
                      decoration: kTextFieldDecration.copyWith(
                          hintText: 'Enter your email')),
                  const SizedBox(
                    height: 8.0,
                  ),
                  TextField(
                      onChanged: (value) {
                        password = value;
                      },
                      decoration: kTextFieldDecration.copyWith(
                          hintText: 'Enter your password')),
                  const SizedBox(
                    height: 24.0,
                  ),
                  RoundedButton(
                    text: 'Log in',
                    color: Colors.lightBlueAccent,
                    onPressed: () async {
                      setState(() {
                        isLoading = true;
                      });
                      try {
                        _auth
                            .signInWithEmailAndPassword(
                                email: email, password: password)
                            .then((value) {
                          User? user = _auth.currentUser;
                          if (user != null) {
                            Navigator.pushReplacementNamed(
                                context, ChatScreen.id);
                          }
                          setState(() {
                            isLoading = false;
                          });
                        });
                      } catch (e) {
                        setState(() {
                          isLoading = false;
                        });
                      }
                    },
                  ),
                ],
              ),
      ),
    );
  }
}
