import 'package:flutter/material.dart';
import '../model/movie.dart';

// ignore: must_be_immutable
class SingleMovie extends StatelessWidget {
  Movie movie;
  // ignore: use_key_in_widget_constructors
  SingleMovie({required this.movie});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          movie.title,
          style: const TextStyle(
            fontSize: 25,
          ),
        ),
      ),
      body: ListView(
        children: [
          Container(
            padding: const EdgeInsets.all(20),
            child: Hero(
              tag: movie.title,
              child: Image.network(
                movie.poster,
                height: 200,
                fit: BoxFit.fitHeight,
              ),
            ),
          ),
          // ignore: avoid_unnecessary_containers
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  '⭐$movie.rating',
                  style: const TextStyle(
                      fontSize: 16, fontWeight: FontWeight.w600),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          details('Directed by', movie.director),
          details('Produced by', movie.production),
          details('Release date', movie.year),
          details('Starring', movie.actors),
          details('Screenplay by', movie.writer),
          details('Language', movie.language)
        ],
      ),
    );
  }
}

Widget details(String tittle, String details) {
  return Container(
    margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          tittle,
          style: const TextStyle(fontSize: 18, fontWeight: FontWeight.w800),
        ),
        const SizedBox(
          height: 10,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 10),
          child: Text(
            details,
            style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
          ),
        ),
      ],
    ),
  );
}
