import 'package:bmi/constants.dart';
import 'package:flutter/material.dart';
import 'reusable_card.dart';
import 'bottom_button.dart';

class ResultsPage extends StatelessWidget {

  ResultsPage({@required this.interpretations,@required this.bmiResult,@required this.resultText});

final String bmiResult;
final String resultText;
final String interpretations;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text ('BMI CALCULATOR')),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Expanded(
              child: Container(
                padding: EdgeInsets.all(15.0),
                alignment: Alignment.bottomLeft,
                child: Text(
                    'Your Result',
                style: kTitleTextStyle,
                ),
              ),
          ),
          Expanded(
            flex: 5,
            child: ReusableCard(colour: kActivecardcolour,
              cardChild: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                      resultText.toUpperCase(),
                  style: kresultTextStyle,
                  ),
                  Text(
                      bmiResult,
                    style: kBMItextstyle,
                  ),
                  Text(
                    interpretations,
                    textAlign: TextAlign.center,
                    style: kBodytextstyle,
                  ),
                ],
              ),),
          ),
          BottomButton( buttontitle: 'RE-CALCULATE',
            onTap: (){
            Navigator.pop(context);
            },
          )
        ],
      )
    );
  }
}
