import 'package:faq_design/model/faq.dart';
import 'package:faq_design/model/faq_data.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import '../constance.dart';

// ignore: use_key_in_widget_constructors
Widget buildPanel() {
  return Consumer<FaqData>(
    builder: (context, faqData, chail) {
      return ListView.builder(
          itemBuilder: (context, index) {
            final faq = faqData.faqs[index];
            return ExpansionTile(
              tilePadding: kBasicMargin.copyWith(
                right: 0,
              ),
              iconColor: Color(0xff222222),
              collapsedIconColor: Color(0xff222222),
              title: Padding(
                padding: kBasicMargin.copyWith(
                    bottom: 10, top: 20, left: 0, right: 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Text(
                            faq.question,
                            style: TextStyle(
                              fontSize: 14,
                              letterSpacing: 0,
                              height: 1.8,
                              color: Color(0xff222222),
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 5),
                          child: Icon(
                            !faq.isExpanded
                                ? FontAwesomeIcons.chevronDown
                                : FontAwesomeIcons.chevronUp,
                            size: 20,
                          ),
                        ),
                      ],
                    ),
                    Visibility(
                      visible: !faq.isExpanded,
                      child: Padding(
                          padding:
                              kBasicMargin.copyWith(top: 10, left: 0, right: 0),
                          child: Container(
                            width: double.infinity,
                            color: Colors.grey,
                            height: 1,
                          )),
                    ),
                  ],
                ),
              ),
              trailing: SizedBox(
                width: 1,
              ),
              children: [
                Container(
                  width: 500,
                  padding: kBasicMargin.copyWith(
                    top: 25,
                    bottom: 25,
                    right: 60,
                  ),
                  color: Color(0xffFFEEDB),
                  child: Text(
                    faq.answer,
                    style: TextStyle(
                      fontSize: 14,
                      height: 1.2,
                      color: Color(0xff4E4E4E),
                    ),
                  ),
                ),
              ],
              initiallyExpanded: faq.isExpanded,
              onExpansionChanged: (bool isExpanded) {
                final faq = faqData.faqs[index];
                faqData.updateExpanded(faq);
              },
            );
          },
          itemCount: FaqData().faqCount());
    },
  );
}
