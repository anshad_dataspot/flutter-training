import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';

import '../data.dart';

// ignore: use_key_in_widget_constructors
class ChartLine extends StatelessWidget {
  final double barWidth = 10;
  @override
  Widget build(BuildContext context) {
    return LineChart(
      LineChartData(
        maxY: 260,
        minY: 0,
        borderData: FlBorderData(
          border: const Border(
            top: BorderSide(width: 0),
            bottom: BorderSide(width: .5),
          ),
        ),
        // groupsSpace: 10,
        titlesData: FlTitlesData(
          // topTitles: BarTitles.getTopBottomTitles(),
          bottomTitles: BarTitles.getTopBottomTitles(),
          leftTitles: BarTitles.getSideTitles(),
          // rightTitles: BarTitles.getSideTitles(),
        ),
        gridData: FlGridData(
          checkToShowHorizontalLine: (value) => value % 50 == 0,
          getDrawingHorizontalLine: (value) {
            if (value == 0) {
              return FlLine(
                color: Colors.grey,
                strokeWidth: 3,
              );
            } else {
              return FlLine(
                color: Colors.grey,
                strokeWidth: 0.8,
              );
            }
          },
        ),
        lineBarsData: [
          LineChartBarData(
            spots: BarData.barData
                .map((data) => FlSpot(data.id.toDouble(), data.value))
                .toList(),
            colors: [Colors.teal.shade900],
          ),
        ],
      ),
    );
  }
}

class BarTitles {
  static SideTitles getTopBottomTitles() => SideTitles(
      showTitles: true,
      getTextStyles: (value) =>
          const TextStyle(color: Colors.black, fontSize: 10),
      margin: 10,
      getTitles: (double id) => BarData.barData
          .firstWhere((element) => element.id == id.toInt())
          .month);

  static SideTitles getSideTitles() => SideTitles(
        showTitles: true,
        getTextStyles: (value) =>
            const TextStyle(color: Colors.black, fontSize: 10),
        rotateAngle: 0,
        interval: 50,
        margin: 20,
        reservedSize: 30,
        getTitles: (double value) => '${value.toInt()}',
      );
}
