import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';

import '../data.dart';

class ChatBar extends StatelessWidget {
  final double barWidth = 10;
  @override
  Widget build(BuildContext context) {
    return BarChart(
      BarChartData(
        maxY: 260,
        minY: 0,

        borderData: FlBorderData(
          border: const Border(
            top: BorderSide(width: 0),
            bottom: BorderSide(width: .5),
          ),
        ),
        // groupsSpace: 10,
        titlesData: FlTitlesData(
          // topTitles: BarTitles.getTopBottomTitles(),
          bottomTitles: BarTitles.getTopBottomTitles(),
          leftTitles: BarTitles.getSideTitles(),
          // rightTitles: BarTitles.getSideTitles(),
        ),
        gridData: FlGridData(
          checkToShowHorizontalLine: (value) => value % 50 == 0,
          getDrawingHorizontalLine: (value) {
            if (value == 0) {
              return FlLine(
                color: Colors.grey,
                strokeWidth: 3,
              );
            } else {
              return FlLine(
                color: Colors.grey,
                strokeWidth: 0.8,
              );
            }
          },
        ),
        barGroups: BarData.barData
            .map(
              (data) => BarChartGroupData(
                x: data.id,
                barRods: [
                  BarChartRodData(
                    y: data.value,
                    width: barWidth,
                    colors: [Colors.orange],
                    borderRadius: const BorderRadius.only(
                      bottomLeft: Radius.circular(0),
                      bottomRight: Radius.circular(0),
                    ),
                  ),
                ],
              ),
            )
            .toList(),
      ),
    );
  }
}

class BarTitles {
  static SideTitles getTopBottomTitles() => SideTitles(
      showTitles: true,
      getTextStyles: (value) =>
          const TextStyle(color: Colors.black, fontSize: 10),
      margin: 10,
      getTitles: (double id) => BarData.barData
          .firstWhere((element) => element.id == id.toInt())
          .month);

  static SideTitles getSideTitles() => SideTitles(
        showTitles: true,
        getTextStyles: (value) =>
            const TextStyle(color: Colors.black, fontSize: 10),
        rotateAngle: 0,
        interval: 50,
        margin: 20,
        reservedSize: 30,
        getTitles: (double value) => '${value.toInt()}',
      );
}
