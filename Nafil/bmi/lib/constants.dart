import 'package:flutter/material.dart';

const kBottomContainerHeight = 80.0;
const kActivecardcolour = Color(0xFF1D1E33);
const kInactivecardcolour = Color(0xFF111328);
const kBottomcontainercolour = Color(0xFFEB1555);

const kTextstylee = TextStyle(
  fontSize: 18.0,
  color: Color(0xFF8D8E98),
);

const kNumberTextstylee =TextStyle(
  fontSize: 50.0,
  fontWeight: FontWeight.w900,
);

const kLargeButtonTextStyle = TextStyle(
  fontSize: 25.0,
  fontWeight: FontWeight.bold,
);

const kTitleTextStyle =TextStyle(
  fontSize: 50.0,
  fontWeight: FontWeight.bold,
);

const kresultTextStyle = TextStyle(
  color: Color(0xFF24D876),
  fontSize: 22.0,
  fontWeight: FontWeight.bold,
);
const kBMItextstyle = TextStyle(
  fontSize: 100.0,
  fontWeight: FontWeight.bold
);

const kBodytextstyle = TextStyle(
  fontSize: 22.0
);