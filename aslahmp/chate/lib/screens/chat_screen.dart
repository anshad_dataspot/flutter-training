import 'package:flutter/material.dart';
import '../constants.dart';
import './welcome_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

final _firestor = FirebaseFirestore.instance;
User? user;

// ignore: use_key_in_widget_constructors
class ChatScreen extends StatefulWidget {
  static String id = 'chat_screen';
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  final messageTextController = TextEditingController();
  String messageText = '';

  final _auth = FirebaseAuth.instance;

  @override
  void initState() {
    super.initState();
    getCurrentUser();
  }

  void getCurrentUser() async {
    user = _auth.currentUser;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: null,
        actions: <Widget>[
          IconButton(
              icon: const Icon(Icons.close),
              onPressed: () async {
                await _auth.signOut().then((value) {
                  Navigator.pushReplacementNamed(context, WelcomeScreen.id);
                });
                Navigator.pushReplacementNamed(context, WelcomeScreen.id);
              }),
        ],
        title: const Text('⚡️Chat'),
        backgroundColor: Colors.lightBlueAccent,
      ),
      body: SafeArea(
          child: Column(
        children: [
          MessageStream(),
          Container(
            decoration: kMessageContainerDecoration,
            width: double.infinity,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: TextField(
                    controller: messageTextController,
                    onChanged: (value) {
                      messageText = value;
                    },
                    decoration: kMessageTextFieldDecoration,
                  ),
                ),
                // ignore: deprecated_member_use
                FlatButton(
                  onPressed: () {
                    messageTextController.clear();
                    _firestor.collection('messages').add({
                      'text': messageText,
                      'sender': user!.email,
                      'time': DateTime.now(),
                    });
                  },
                  child: const Text(
                    'Send',
                    style: kSendButtonTextStyle,
                  ),
                ),
              ],
            ),
          ),
        ],
      )),
    );
  }
}

bool isMe = false;

// ignore: use_key_in_widget_constructors
class MessageStream extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: StreamBuilder<QuerySnapshot>(
          stream: _firestor
              .collection('messages')
              .orderBy('time', descending: true)
              .snapshots(),
          builder: (context, snapshot) {
            List<MessageBubble> messagesWidgets = [];
            if (snapshot.hasData) {
              final messages = snapshot.data!.docs;

              for (var message in messages) {
                final messageText = message.get('text');
                final messageSender = message.get('sender');
                if (user!.email == messageSender) {
                  isMe = true;
                } else {
                  isMe = false;
                }
                final messagesBubble = MessageBubble(
                  text: messageText,
                  sender: messageSender,
                  isMe: isMe,
                );
                messagesWidgets.add(messagesBubble);
              }
            }
            // ignore: sized_box_for_whitespace
            return Container(
              height: MediaQuery.of(context).size.height,
              child: ListView(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                reverse: true,
                children: messagesWidgets,
              ),
            );
          }),
    );
  }
}

class MessageBubble extends StatelessWidget {
  final bool isMe;
  final String sender;
  final String text;
  // ignore: use_key_in_widget_constructors
  const MessageBubble(
      {required this.text, required this.sender, required this.isMe});

  @override
  Widget build(BuildContext context) {
    // ignore: prefer_const_constructors

    return Container(
        // height: 40,
        margin: const EdgeInsets.only(top: 10, left: 10, right: 10),
        padding: const EdgeInsets.only(left: 0),
        child: Column(
          crossAxisAlignment:
              isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: Text(
                sender,
                style: const TextStyle(fontSize: 12.0, color: Colors.black54),
              ),
            ),
            Material(
              elevation: 5,
              borderRadius: BorderRadius.only(
                  topLeft: isMe
                      ? const Radius.circular(20)
                      : const Radius.circular(0),
                  topRight: const Radius.circular(20),
                  bottomLeft: const Radius.circular(20),
                  bottomRight: isMe
                      ? const Radius.circular(0)
                      : const Radius.circular(20)),
              color: isMe ? Colors.lightBlueAccent : Colors.blueAccent,
              child: Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 10.0, horizontal: 20.0),
                child: Text(
                  '$text ',
                  style: const TextStyle(
                      color: Colors.white,
                      fontSize: 15,
                      fontWeight: FontWeight.w600),
                ),
              ),
            ),
          ],
        ));
  }
}
