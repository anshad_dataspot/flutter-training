class Movie {
  final String poster;
  final String title;
  final String year;
  final String rating;
  final String director;
  final String production;
  final String language;
  final String writer;
  final String actors;

  Movie({
    required this.title,
    required this.poster,
    required this.year,
    required this.director,
    required this.actors,
    required this.language,
    required this.production,
    required this.rating,
    required this.writer,
  });

  factory Movie.fromJson(Map<String, dynamic> json) {
    return Movie(
      poster: json["Poster"],
      title: json["Title"],
      year: json["Released"],
      writer: json["Writer"],
      actors: json["Actors"],
      rating: json["imdbRating"],
      director: json["Director"],
      language: json["Language"],
      production: json["Production"],
    );
  }
}
