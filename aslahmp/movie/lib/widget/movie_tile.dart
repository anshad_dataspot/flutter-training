import 'package:flutter/material.dart';
import 'package:movie/screens/single_movie_screen.dart';

import '../model/movie.dart';

class MovieTile extends StatelessWidget {
  final Movie movie;

  // ignore: use_key_in_widget_constructors
  const MovieTile({required this.movie});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        margin: const EdgeInsets.only(left: 10, right: 10, bottom: 5, top: 5),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Hero(
              tag: movie.title,
              child: Image.network(
                movie.poster,
                width: 100,
              ),
            ),
            Expanded(
                child: Container(
              margin: const EdgeInsets.only(left: 10, top: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '⭐${movie.rating}',
                    style: const TextStyle(
                        fontSize: 16, fontWeight: FontWeight.w400),
                  ),
                  Text(
                    movie.title,
                    style: const TextStyle(
                        fontSize: 25, fontWeight: FontWeight.w500),
                  ),
                  Text(
                    'Release: ${movie.year}',
                    style: const TextStyle(fontSize: 16),
                  ),
                  Text(
                    'Director: ${movie.director}',
                    style: const TextStyle(fontSize: 16),
                  ),
                ],
              ),
            ))
          ],
        ),
      ),
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (context) {
          return SingleMovie(
            movie: movie,
          );
        }));
      },
    );
  }
}

// // ignore: use_key_in_widget_constructors
// // ignore: must_be_immutable
// class MovieTile extends StatefulWidget {
//   String moviName;
//   // ignore: use_key_in_widget_constructors
//   MovieTile({required this.moviName});
//   @override
//   State<MovieTile> createState() => _MovieTileState();
// }

// class _MovieTileState extends State<MovieTile> {
//   bool isLoding = true;
//   String imageUrl = '';
//   String name = '';
//   String rating = '';
//   String year = '';
//   String director = '';
//   late Movie movie;
//   @override
//   void initState() {
//     super.initState();
//     updateUi(widget.moviName);
//   }

//   void updateUi(dynamic moviName) async {
//     NetworkHelper networkHelper = NetworkHelper(name: moviName);
//     var moviData = await networkHelper.getMovie();
//     movie = Movie.fromJson(moviData);

//     name = movie.title;
//     imageUrl = movie.poster;
//     rating = movie.rating;
//     year = movie.year;
//     director = movie.director;
//     setState(() {
//       isLoding = false;
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return isLoding
//         ? const SizedBox()
//         :
//   }
// }
