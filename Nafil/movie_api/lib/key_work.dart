import 'package:http/http.dart' as http;
import 'dart:convert';



class NetworkHelper {
  String key = '9d993451';
  String url = 'https://www.omdbapi.com/?apikey=';

  Future getMovie(String name) async {
    try {
      http.Response response = await http.get(
        Uri.parse('$url$key&t=$name'),
      );
      if (response.statusCode == 200) {
        String data = response.body;

        return jsonDecode(data);
      } else {
        // ignore: avoid_print
        print(response.statusCode);
        return Error;
      }
    } catch (e) {
      return Error;
    }
  }
}

