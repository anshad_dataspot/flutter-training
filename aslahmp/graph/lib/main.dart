import 'package:flutter/material.dart';

import './bar_chart/quantity__chart.dart';
import './line_chart/amunt_chart.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Chart Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Scaffold(
        body: Stack(
          children: [
            Positioned(
              top: 20,
              left: 20,
              child: QuantityChart(),
            ),
            Positioned(
              top: 380,
              left: 20,
              child: AmountChart(),
            ),
            Positioned(
              top: 330,
              left: 20,
              child: Row(
                children: const [
                  Text(
                    'Amouunt spent on fuel',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w800),
                  ),
                  // DropdownButton(items: year),
                ],
              ),
            ),
            Positioned(
              left: 10,
              top: 550,
              // top: 220,
              // width: 50,
              // padding: const EdgeInsets.only(left: 20),
              child: Transform(
                child: const SizedBox(
                  child: Text(
                    'in Rupees(10k)',
                    style: TextStyle(fontSize: 12),
                  ),
                ),
                transform: Matrix4.identity()..rotateZ(-90 * 3.1415927 / 180),
              ),
            ),
          ],
        ),
      ),
    );
    // This trailing comma makes auto-formatting nicer for build methods.
  }
}

class FuelQantity {
  String month;
  double quantity;
  FuelQantity({required this.quantity, required this.month});
}
