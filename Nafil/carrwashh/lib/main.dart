

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: ListViewHomePage(),
    );
  }
}

class ListViewHomePage extends StatefulWidget {

  @override
  _ListViewHomePageState createState() => _ListViewHomePageState();
}

class _ListViewHomePageState extends State<ListViewHomePage> {
  var titlelist = [
    "Smart Car Wash",
    "Al Aimtiaz Wash",
    "Super Polish a Car - UAQ",
    "Over Boost Car Wash"
  ];
  var desclist = [
    "Caring for your vehicle is our business",
    "Wash your car at your place",
    "We serve you with the latest equipment and the highest quality",
    "Our car washing service is the name of excellence"
  ];
  var imglist = [
        "assets/football.jpg",
        "assets/grunge football background 1305.jpg",
        "assets/toy car.jpg",
        "assets/toy truck.jpg"
  ];
  var btnlist = [
    RaisedButton(
        onPressed:() {},
      color: Colors.orangeAccent,

      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30.0),
      ),
        child: Text("Closed"),
      ),


    RaisedButton(
      onPressed:() {},
      color: Colors.orangeAccent,

      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30.0),

      ),
      child: Text("Busy"),
    ),
    RaisedButton(
      onPressed:() {},
      color: Colors.orangeAccent,

      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30.0),
      ),
      child: Text("Order now"),
    ),
    RaisedButton(
      onPressed:() {},
      color: Colors.orangeAccent,

      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30.0),
      ),
      child: Text("Closed"),
    ),

  ];

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width * 0.4;
    return Scaffold(
      appBar: AppBar(
        title: Text(
            "Service Providers",
        style: TextStyle(color: Colors.deepOrangeAccent),
        ),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.white,

      ),
      body: ListView.builder(
        itemCount: desclist.length,
          itemBuilder: (context, index){
          return GestureDetector(
            onTap: () {

            },

            child: Card(
                child: Row(
                  children: <Widget>[
                    Container(
                      width: 100,
                      height: 150,
                      child: Image.asset(imglist[index]),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            titlelist[index],
                            style: TextStyle(
                              fontSize: 13,
                              color: Colors.orangeAccent,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(height: 10,),
                          Container(
                            width: width,
                            child: Text(
                              desclist[index],
                              style: TextStyle(
                                fontSize: 10.0,
                                color: Colors.orangeAccent,
                              ),
                            ),
                          ),

                         Row(
                           children: <Widget>[
                             Icon(
                               Icons.star_border,
                             ),
                             Icon(
                                 Icons.star_border,
                             ),
                             Icon(
                               Icons.star_border,
                             ),
                             Icon(
                               Icons.star_border,
                             ),
                             Icon(
                               Icons.star_border,
                             ),
                           ],
                         ),

                        ],
                      ),
                    ),
                    Container(

                      child: btnlist[index],
                    ),
                  ],
                )
            ),
          );
          }

      )
    );
  }
}

