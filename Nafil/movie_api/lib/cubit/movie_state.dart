part of 'movie_cubit.dart';

@immutable
abstract class MovieState {}

class MovieLoading extends MovieState {}

class MovieLoaded extends MovieState {
  final List<Movie> movies;
  MovieLoaded(this.movies);
  List<Object> get props => [movies];
}

class MovieError extends MovieState {}

