import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:odmb/cubic_home.dart';
import 'package:odmb/models/movie.dart';
import 'package:omdb_dart/omdb_dart.dart';

part 'movie_state.dart';

List movieNames = [];

class MovieCubit extends Cubit<MovieState> {
  MovieCubit(CubicHome cubicHome) : super(MovieInitial());
  getData() async {
    emit(MovieLoading());
    List<MovieModel> movies = [];
    movieNames.add(MovieNameModel(name: "Batman"));

    movieNames.add(MovieNameModel(name: "Parasite"));
    movieNames.add(MovieNameModel(name: "Loki"));
    movieNames.add(MovieNameModel(name: "Avengers"));
    movieNames.add(MovieNameModel(name: "Death Race"));
    movieNames.add(MovieNameModel(name: "Don't Breathe"));
    movieNames.add(MovieNameModel(name: "Army Of The Dead"));
    movieNames.add(MovieNameModel(name: "The Mechanic"));
    for (int i = 0; i < movieNames.length; i++) {
      Omdb client = Omdb("be4ab7ee", ((movieNames[i] as MovieNameModel).name));
      await client.getMovie();
      movies.add(MovieModel(
          title: client.movie.title,
          actors: client.movie.actors,
          runtime: client.movie.runtime,
          language: client.movie.language,
          plot: client.movie.plot,
          rating: client.movie.imdbRating,
          votes: client.movie.imdbVotes,
          poster: client.movie.poster));
    }
    emit(MovieLoaded(movie: movies));
  }
}
